from django.db import models

class Bootcamp(models.Model):
    titulo = models.CharField(max_length=255)
    profesor = models.TextField()
    descripcion = models.TextField()
    completado = models.BooleanField(default=False)

    def __str__(self):
        return self.titulo