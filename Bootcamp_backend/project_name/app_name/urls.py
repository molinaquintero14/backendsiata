from django.urls import path
from .views import ListBootcamp, DetailBootcamp

urlpatterns = [
    path('bootcamp/', ListBootcamp.as_view(), name='Lista de Bootcamps'),
    path('bootcamp/<int:pk>/', DetailBootcamp.as_view(), name='Detalle de Bootcamp'),
]
