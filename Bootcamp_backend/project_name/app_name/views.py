from rest_framework.response import Response
from rest_framework import status
from .models import Bootcamp
from .serializers import BootcampSerializer
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView

class ListBootcamp(APIView):
    def post(self, request):
        serializer = BootcampSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get(self, request):
        bootcamps = Bootcamp.objects.all()
        serializer = BootcampSerializer(bootcamps, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
class DetailBootcamp(APIView):
    def delete(self, request, pk):
        bootcamp = get_object_or_404(Bootcamp, pk=pk)
        bootcamp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def put(self, request, pk):
        bootcamp = get_object_or_404(Bootcamp, pk=pk)
        serializer = BootcampSerializer(bootcamp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)